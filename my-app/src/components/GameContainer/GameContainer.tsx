import React from 'react';
import PlayersCredential from "../PlayersCredential";
import GameTable from "../GameTable";
import {useSelector} from "react-redux";
import {IRootState} from "../../utils/types";

const GameContainer = () => {
    const isInGame = useSelector((state: IRootState) => state.players.isInGame)

    return (
        <div>
            {isInGame ?
                <GameTable/>
                :
                <PlayersCredential/>
            }
        </div>
    );
};

export default GameContainer;