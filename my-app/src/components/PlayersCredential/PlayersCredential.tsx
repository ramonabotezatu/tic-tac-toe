import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {startGame} from "../../reducer/players";
import styles from './PlayersCredential.module.scss';
import NameInput from "../../commonComponents/NameInput";

const PlayersCredential = () => {
    const [players, setPlayers] = useState({firstPlayer: "", secondPlayer: ""})
    const dispatch = useDispatch()

    const startGameWithPlayers = (e: any) => {
        e.preventDefault();
        if (players.firstPlayer && players.secondPlayer) {
            dispatch(startGame(players));
        }

    }

    const onInputChange = (e: any, key: string) => {
        setPlayers((prevState) => {
            return ({
                ...prevState,
                [key]: e.target.value
            });
        })
    }

    return (
        <div className={styles.container}>
            <form className={styles.form} onSubmit={e => startGameWithPlayers(e)}>
                <NameInput
                    customStyle={styles.input}
                    name={"firstPlayer"}
                    placeholder={"Player 1"}
                    id={"fn1Name"}
                    onChange={onInputChange}
                />
                <NameInput
                    customStyle={styles.input}
                    name={"secondPlayer"}
                    placeholder={"Player 2"}
                    id={"fn2Name"}
                    onChange={onInputChange}
                />

                <input className={styles.submitBtn} type="submit" value="Start Game"/>
            </form>
        </div>
    );
};

export default PlayersCredential;