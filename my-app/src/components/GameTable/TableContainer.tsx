import React, {useEffect, useState} from 'react';
import styles from './GameTable.module.scss'
import {setWinner} from "../../reducer/players";
import {useDispatch} from "react-redux";
import {IPlayerState} from "../../utils/types";
import {checkTie, checkWin} from "../../utils/functions";

interface IProps {
    playerState: IPlayerState;
}
const TableContainer = ({playerState}: IProps) => {
    const {gameCount, winner, firstPlayer, secondPlayer} = playerState
    const [data, setData] = useState(['', '', '', '', '', '', '', '', ''])
    const [currentTurn, setCurrentTurn] = useState(0);
    const dispatch = useDispatch()

    const draw = (index: number) => {
        if (winner) return;
        if (data[index - 1] === '') {
            const current = currentTurn === 0 ? "X" : "O"
            data[index - 1] = current;
            setCurrentTurn(currentTurn === 0 ? 1 : 0)
        }

        if (checkWin(data)) {
            const winner = currentTurn === 0 ? `${firstPlayer}` : `${secondPlayer}!`
            dispatch(setWinner({winner}));
        } else if (checkTie(data)) {
            dispatch(setWinner({winner: "NOBODY"}));
        }
    }

    useEffect(() => {
        setData(['', '', '', '', '', '', '', '', '']);
        setCurrentTurn(0);
        setWinner('');
    }, [gameCount]);


    return (
        <div className={styles.tableContainer}>
            <div className={styles.col}>
                <span className={styles.cell} onClick={() => draw(1)}>
                    {data[0]}
                </span>
                <span className={styles.cell} onClick={() => draw(2)}>
                    {data[1]}
                </span>
                <span className={styles.cell} onClick={() => draw(3)}>
                    {data[2]}
                </span>
            </div>
            <div className={styles.col}>
                <span className={styles.cell} onClick={() => draw(4)}>
                    {data[3]}
                </span>
                <span className={styles.cell} onClick={() => draw(5)}>
                    {data[4]}
                </span>
                <span className={styles.cell} onClick={() => draw(6)}>
                    {data[5]}
                </span>
            </div>
            <div className={styles.col}>
                <span className={styles.cell} onClick={() => draw(7)}>
                    {data[6]}
                </span>
                <span className={styles.cell} onClick={() => draw(8)}>
                    {data[7]}
                </span>
                <span className={styles.cell} onClick={() => draw(9)}>
                    {data[8]}
                </span>
            </div>
        </div>
    );
};

export default TableContainer;