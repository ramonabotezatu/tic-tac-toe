import React from 'react';
import styles from './GameTable.module.scss';

interface IProps {
    firstName: string;
    secondName: string;
}

const ActivePlayers = ({firstName, secondName}: IProps) => {
    return (
        <div className={styles.activePlayerContainer}>
            <div>Player 1: <span> {firstName} </span></div>
            <div>Player 2: <span>{secondName}</span></div>
        </div>
    );
};

export default ActivePlayers;