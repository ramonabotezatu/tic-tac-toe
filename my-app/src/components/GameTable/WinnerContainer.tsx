import React from 'react';
import {closeGame, resetGame} from "../../reducer/players";
import {useDispatch} from "react-redux";
import styles from './GameTable.module.scss';

interface IProps {
    winner: string;
}

const WinnerContainer = ({winner}: IProps) => {
    const dispatch = useDispatch()

    return (
        <div className={styles.winnerContainer}>
            <div className={styles.winnerInfo}>The winner is: <span>{winner}</span></div>
            <button className={styles.resetBtn} onClick={() => dispatch(resetGame())}>Play again</button>
            <button className={styles.resetBtn} onClick={() => dispatch(closeGame())}>Close game</button>
        </div>
    );
};

export default WinnerContainer;