import { useSelector } from 'react-redux'
import {IRootState} from "../../utils/types";
import ActivePlayers from "./ActivePlayers";
import TableContainer from "./TableContainer";
import React from "react";
import WinnerContainer from "./WinnerContainer";

const GameTable = () => {
    const playerState= useSelector((state: IRootState) => state.players)

    return (
        <div>
            <div>Game: {playerState.gameCount + 1}</div>
            <ActivePlayers firstName={playerState.firstPlayer} secondName={playerState.secondPlayer}/>
            <TableContainer playerState={playerState}/>
            {playerState.winner && <WinnerContainer winner={playerState.winner}/>}
        </div>
    );
};

export default GameTable;