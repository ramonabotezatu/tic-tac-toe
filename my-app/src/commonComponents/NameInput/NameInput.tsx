import React, {ChangeEvent} from 'react';

interface IProps {
    name: string;
    placeholder: string
    id: string;
    customStyle: string;
    onChange: (e: ChangeEvent<HTMLInputElement>, key: string) => void

}

const NameInput = ({name, placeholder, id, customStyle, onChange}: IProps) => {
    return (
        <input
            type="text"
            id={id}
            name={name}
            placeholder={placeholder}
            maxLength={10}
            className={customStyle}
            onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e, name)}
        />
    );
};

export default NameInput;