import {createSlice} from '@reduxjs/toolkit'
import {initialState} from "../store/initialState";

export const playersSlice = createSlice({
    name: 'players',
    initialState: initialState,
    reducers: {
        startGame: (state, {payload}) => {
            state.isInGame = true;
            state.firstPlayer = payload?.firstPlayer;
            state.secondPlayer = payload?.secondPlayer;
            state.winner = '';
        },
        setWinner: (state, {payload}) => {
            state.winner = payload.winner;
        },
        closeGame: (state) => {
            state.isInGame = false;
            state.startGame = false;
            state.winner = '';
            state.gameCount = 0;
        },
        resetGame: (state) => {
            state.gameCount = state.gameCount + 1;
            state.winner = '';
        },
    },
})

export const {startGame, resetGame, setWinner, closeGame} = playersSlice.actions

export default playersSlice.reducer