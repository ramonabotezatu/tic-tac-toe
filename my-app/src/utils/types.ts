export interface IRootState {
    players: IPlayerState,
}

export interface IPlayerState {
    value: number;
    firstPlayer: string;
    secondPlayer: string;
    startGame: boolean;
    winner: string;
    isInGame: boolean;
    gameCount: number;
}