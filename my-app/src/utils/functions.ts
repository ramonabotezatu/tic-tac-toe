export const checkWin = (data: string[]): boolean => {
    return checkRow(data) || checkCol(data) || checkDiagonal(data);
}
export const checkRow = (data: string[]) => {
    for (let i = 0; i < 9; i += 3) {
        if (data[i] === data[i + 1] &&
            data[i] === data[i + 2] &&
            data[i] !== '') {
            return true;
        }
    }
    return false;
}
export const checkCol = (data: string[]) => {
    for (let i = 0; i < 3; i++) {
        if (data[i] === data[i + 3] &&
            data[i] === data[i + 6] &&
            data[i] !== '') {
            return true;
        }
    }
    return false;
}
export const checkTie = (data: string[]) => {
    let count = 0;
    data.forEach((cell) => {
        if (cell !== '') {
            count++;
        }
    })
    return count === 9;
}

export const checkDiagonal = (data: string[]) => {
    return ((data[0] === data[4] && data[0] === data[8] && data[0] !== '')
        || (data[2] === data[4] && data[2] === data[6] && data[2] !== ''));
}