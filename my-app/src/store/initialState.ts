import {IPlayerState} from "../utils/types";

export const initialState: IPlayerState = {
    value: 0,
    isInGame: false,
    startGame: false,
    firstPlayer: "",
    secondPlayer: "",
    winner: '',
    gameCount: 0,
};
