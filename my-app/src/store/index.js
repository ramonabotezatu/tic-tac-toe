import { configureStore } from '@reduxjs/toolkit'
import playersReducer from '../reducer/players'

export default configureStore({
    reducer: {
        players: playersReducer ,
    },
})