import './App.css';
import GameContainer from "./components/GameContainer";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Tic tac toe game</h1>
                <GameContainer/>
            </header>
        </div>
    );
}
export default App;
